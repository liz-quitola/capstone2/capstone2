const Order = require ("../models/Order");
const Scent = require ("../models/Scent");
const User = require("../models/User");

// Create Order - NON-Admin
module.exports.checkout = async (data) => {
	console.log(data)

	let product = await Scent.findById(data.productId);

	let newOrder = await new Order({
		userId: data.userId,
		productId: data.productId,
		productName: product.productName,
		price: product.price,
		quantity: data.quantity,
		totalAmount: data.quantity * product.price
	})

	return newOrder.save().then((order, error) =>{

		if (error){
				return false
			} else {
				return order
			}
	})
}

// Retrieving All Orders - Admin Only
module.exports.getOrders = () => {
		return Order.find({}).then(result =>{
			console.log(result)
			return result
		})
	}

// Retrieving Authenticated User's Order - User
module.exports.getMyOrders = (reqBody) => {
		return Order.find({userId:reqBody}).then(result =>{
			console.log(result)
			return result
		})
	}
