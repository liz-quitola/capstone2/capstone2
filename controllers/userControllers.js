const User = require("../models/User");
const Scent = require ("../models/Scent");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error){
			return false
		} else {
			return true
		}
	})
}

// Retrieving all users
module.exports.getAllProfile = () =>{
	return User.find({}).then(result =>{
		result.password = "";
		return result;
	});
}


// User authentication 
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{

console.log(reqBody.email)
console.log(reqBody.password)
console.log(result)

		if(result == null){
			return false
		} else {

			//compareSync(dataToBeCompared, encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect) {
				return { access: auth.createAccessToken(result)}
			} else {
				return false;
			}
		}
	})
}

// Set User as Admin
module.exports.updateAsAdmin = (reqParams, data) => {
	console.log(data)

	return User.findById(reqParams).then((result, error) =>{
		console.log(result)

		if(data){
			
			result.isAdmin = true

			return result.save().then ((result, error) =>{

				if(error){
					return false
				} else {
					return result
				}
			})

		} else {
			return "Request NOT granted. User NOT an Admin."
		}

	})
}


module.exports.checkEmailExists = (reqBody) => {

	//The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email: reqBody.email}).then( result => {

		//the "find" method returns a record if a match is found
		if(result.length > 0) {
			return true

		//No duplicate email found
		//The user is not yet registered in the database
		} else {
			return false
		}
	})
}