const Scent = require ("../models/Scent");
const User = require("../models/User");
const Order = require("../models/Order");

// Create a product - Admin only
module.exports.addProduct = (reqBody) =>{

	let newProduct = new Scent ({

		productName: reqBody.productName,
		inspiredBy: reqBody.inspiredBy,
		category: reqBody.category,
		description: reqBody.description,
		size: reqBody.size,
		price: reqBody.price
	})

	return newProduct.save().then((scent, error)=>{
		
		if(error) {
			return "Request NOT granted. User NOT an Admin."
		} else {
			return true
		}

	})
}

// Retrieving all active products
module.exports.getActiveProducts = () => {

	return Scent.find({isActive: true}).then(result => {
		return result
	})
}

// Retrieving a single product
module.exports.getProduct = (reqParams) => {

	return Scent.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Updating a product info - Admin only
module.exports.updatedProduct = (data) => {
	console.log(data);

	return Scent.findById(data.productId).then((result, error) =>{

		console.log(result);

		if(data){
			result.productName = data.updatedProduct.productName
			result.price = data.updatedProduct.price
			result.isActive = data.updatedProduct.isActive

			console.log(result)

			return result.save().then((updatedProduct, error) =>{

				if(error){
					return "Request NOT granted. User NOT an Admin."
				}else{
					return updatedProduct
				}
			})
		}

	})
}

//  Archiving product - Admin only
module.exports.archiveProduct = (reqParams, data) => {
	console.log(data)

	return Scent.findById(reqParams.productId).then((result, error) => {

		if(data) {

			result.isActive = false

			return result.save().then((archivedProduct, error) => {

				if(error) {

					return false;

				} else {

					return archivedProduct;
				}
			})

		} else {

			return "Request NOT granted. User NOT an Admin."
		}

	})
}