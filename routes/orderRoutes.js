const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth");



// Router for Create Order - NON-Admin
router.post("/users/checkout", auth.verify, (req, res) =>{
	console.log(req.body)

	let data = {
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			userId: auth.decode(req.headers.authorization).id,
			productId: req.body.productId,
			quantity: req.body.quantity
			// totalAmount: req.body.totalAmount
	}
	if(data.isAdmin === false){
	orderControllers.checkout(data).then(resultFromController => res.send(resultFromController));
	} else { 
		res.send("Admins are not allowed to this access. Please Log-in as User.")
	}
})


// Route for Retrieving All Orders - Admin Only
router.get("/allorders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin){
		orderControllers.getOrders(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Request NOT granted. User NOT an Admin.")
	}

	
})


// Route for Retrieving Authenticated User's Order - User
router.get("/myorders", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin){
		res.send("Request NOT granted. This is an Admin account.Only REGULAR USERS are allowed to login here.")
	} else {
		orderControllers.getMyOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}

	
})










module.exports = router;