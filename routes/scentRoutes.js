const express = require("express");
const router = express.Router();
const scentControllers = require("../controllers/scentControllers");
const auth = require("../auth");

// Route to create a product -Admin only
router.post("/products", auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization).isAdmin
	
	if(userData){
		scentControllers.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Request NOT granted. User NOT an Admin.")
	}

	
})

// Route for retrieving all active products
router.get("/products", (req, res) => {

	scentControllers.getActiveProducts().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a single product
router.get("/products/:productId", (req, res) =>{
	console.log(req.params);

	scentControllers.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Route for updating a product info - Admin only
router.put("/products/:productId", auth.verify, (req, res) =>{

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productId: req.params.productId,
		updatedProduct: req.body
	}

	if(data.isAdmin === true){

	scentControllers.updatedProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send("Request NOT granted. User NOT an Admin.")
	}
})



// Route for archiving product
router.put("/products/:productId/archive", auth.verify, (req, res)=>{

	const data = auth.decode(req.headers.authorization).isAdmin
	console.log(data);

	scentControllers.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController))
})





module.exports = router;