const express= require ("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// Route for user registration
router.post("/register", (req, res) =>{
	userControllers.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for retrieving all users
router.get("/allusers", auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	userControllers.getAllProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});


// Route for user authentication
router.post("/login", (req, res) =>{
	userControllers.loginUser(req.body).then(resultFromController =>res.send(resultFromController));
})


// Route for set User as Admin
router.put("/:userId/setasadmin", auth.verify, (req, res) =>{

	const data = auth.decode(req.headers.authorization).isAdmin
	console.log(data);	

	userControllers.updateAsAdmin(req.params.userId, data).then(resultFromController => res.send(resultFromController));
})


router.post("/checkEmail", (req, res) => {
	userControllers.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})






module.exports = router;
