const mongoose = require ("mongoose");
const orderSchema = new mongoose.Schema({

	userId: {
			type: String,
			required: [true, "User ID is required"]
	},

	productName: {
		type: String,
		required: [true, "Name of Scent is required."]
	},

	price: {
		type: Number,
		required: true
	},

	quantity: {
		type: Number,
		required: [true, "At least 1 quantity is required"]
	},

	totalAmount: {
		type: Number,
		required: true
	},

	purchasedOn: {
		type: Date,
		default: new Date ()
	}

})

module.exports = mongoose.model("Order", orderSchema);