const mongoose = require ("mongoose");
const scentSchema = new mongoose.Schema({

	productName: {
		type: String,
		required: [true, "Product Name is required."]
	},
	inspiredBy:{
		type: String,
		required: [true, "Inspiration is required"]
	},

	category: {
		type: String,
		required: [true, "Category is required"]
	},

	description: {
		type: String,
		required: [true, "Description is required"]
	},
	size: {
		type: String,
		required: [true, "Size is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date ()
	},

	
})

module.exports = mongoose.model("Scent", scentSchema);