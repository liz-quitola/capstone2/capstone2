const express = require ("express");
const mongoose = require ("mongoose");
const cors = require ("cors");
const port = 4000;
const app = express();
const userRoutes = require ("./routes/userRoutes");
const scentRoutes = require ("./routes/scentRoutes");
const orderRoutes = require ("./routes/orderRoutes");

mongoose.connect("mongodb+srv://Admin:admin123@course-booking.cevr7.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewURLParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on('error', ()=> console.error.bind(console, 'Connection Error'));
db.once('open', ()=> console.error.bind(console, 'Now connected in MongoDB Atlas'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use("/users", userRoutes);
app.use("/scents", scentRoutes);
app.use("/orders", orderRoutes);


app.listen(process.env.PORT || port, ()=>{
	console.log(`API is now online on port ${process.env.PORT || port}`)

});